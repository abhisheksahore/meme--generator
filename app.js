const image_url = document.getElementById('image-url')
const top_text = document.getElementById('top-text')
const bottom_text = document.getElementById('bottom-text')
const form = document.querySelector('form')
const memes_container = document.querySelector('.memes-container');
const error = document.querySelector('.error');






console.log(memes_container)





const createMeme = e => {
    e.preventDefault();
    

    if (image_url.value !== '' && top_text !== '' && bottom_text !== '') {
    
        if (error.classList.contains('visible')) {
            error.classList.remove('visible');
            error.classList.add('invisible');
        }
        const meme = document.createElement('div');
        const image = document.createElement('img');
        image.classList.add('image-cls');
        const h3_1 = document.createElement('h1');
        const h3_2 = document.createElement('h1');

        

        // for meme container
        meme.classList.add('meme');
        // meme.style.cssText = ``;
        

        // for meme image
        image.setAttribute('src', image_url.value);
        image.style.cssText = `width: 100%; display: block; border-radius: 20px;`;
        

        // for meme top text.
        h3_1.innerText = top_text.value;
        // console.log(h3_1.innerText);
        h3_1.classList.add('meme-text-top')
        // h3_1.style.cssText = ``;
        
        
        // for meme bottom text
        h3_2.innerText = bottom_text.value;
        h3_2.classList.add('meme-text-bottom')
        // h3_2.style.cssText = ``;

        

        // appending to UI
        memes_container.appendChild(meme);
        meme.appendChild(image);
        meme.appendChild(h3_1);    
        meme.appendChild(h3_2);    
        form.reset();
    } else {
        console.log(error.classList.contains('visible'));
        if(!error.classList.contains('visible')){
            error.classList.add('visible');
            error.classList.remove('invisible');
        }
    }
    
}

const removeMeme = e => {
    if (e.target.tagName === "IMG") {
        e.target.parentElement.remove();
    }
}




form.addEventListener('submit', createMeme);
memes_container.addEventListener('click', removeMeme);